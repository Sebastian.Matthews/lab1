package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        boolean game = true;
        while (game) {
            System.out.println("Let's play round " + roundCounter);
            
            int r = (int)Math.round(Math.random()*2);
            String c = rpsChoices.get(r);
            
            while (true) {
                String p = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
                if (!((p.equals("rock")||p.equals("paper")||p.equals("scissors")))) {
                    System.out.println("I do not understand cardboard. Could you try again?");
                    continue;
                } else if ((p.equals("rock") && c.equals("scissors"))||(p.equals("paper") && c.equals("rock"))||(p.equals("scissors") && c.equals("paper"))) {
                    System.out.println("Human chose "+p+", computer chose "+c+". Human wins!");
                    humanScore++;
                    roundCounter++;
                    break;
                } else if ((c.equals("rock") && p.equals("scissors"))||(c.equals("paper") && p.equals("rock"))||(c.equals("scissors") && p.equals("paper"))) {
                    System.out.println("Human chose "+p+", computer chose "+c+". Computer wins!");
                    computerScore++;
                    roundCounter++;
                    break;
                } else if (p.equals(c)){
                    System.out.println("Human chose "+p+", computer chose "+c+". It's a tie!");
                    roundCounter++;
                    break;
                }
            }
            System.out.println("Score: human "+humanScore+", computer " + computerScore);
            while (true) {
                String agian = readInput("Do you wish to continue playing? (y/n)?");
                if (agian.equals("y")) {
                    break;
                } else if (agian.equals("n")) {
                    System.out.println("Bye bye :)");
                    game = false;
                    break;
                } else {
                    continue;
                }
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
